# README #

Repo para generar el contenedor docker de MongoDB para el proyecto de la Tech University

### How do I get set up? ###

Para crear el contenedor:
> docker build -t <usuarioDocker>/<nombreImagen> .

Para ejecutarlo, se recomienda crear primero una red en docker. Una vez hecho:
> docker run -v /opt/datosmongo:/data/db -p 27017:27017 --net redtechu --name servermongodb  <usuarioDocker>/<nombreImagen>

Una vez lo hemos hecho una vez, podemos volver a ejecutar la imagen con:
> docker start servermongodb

Para pararlo:
> docker stop servermongodb

### Who do I talk to? ###

* Pablo Bezon